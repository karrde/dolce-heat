# Change Log
All significant changes made to this project will be document in this file.

This project uses [Semantic Versioning](https://semver.org).

For information regarding all development, see [Milestones](https://gitlab.com/karrde/dolce-heat/-/milestones) and [Issues](https://gitlab.com/karrde/dolce-heat/issues).

## [0.0.1 - John of England](https://gitlab.com/karrde/dolce-heat/-/milestones/1) (2020-01-05)
### Added
 - License
 - Initial Read Me
 - Initial Change Log