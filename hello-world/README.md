# hello world
Dummy project to demonstrate development of software for the arduino in eclipse.

# Usage
Requires:
 - [Arduino Software](https://www.arduino.cc/en/main/software) is installed (for the drivers)
 - [Eclipse](https://www.eclipse.org/downloads/packages/installer) with [Eclipse C++ IDE for Arduino](http://marketplace.eclipse.org/content/eclipse-c-ide-arduino)

Follow [this simple guide](https://www.eclipse.org/community/eclipse_newsletter/2018/july/arduino.php) for further setup steps.