# Dedication
For my love, Naomi.
You are my inspiration and my courage.
The world is darker without you, you were taken from us far too soon.

I hope you're out there somewhere, rolling your eyes at me,
as I attempt to explain this little project.

You will always be missed.

# Dolce Heat
Dolce Heat is a consumer grade, wireless temperature monitoring system.

Currently under slow, active development.

# Description
TBA 

# Installation
TBA

# Usage
TBA

# Support
TBA

# Roadmap
TBA

# Contributing
TBA

# License
Please see the LICENSE file.